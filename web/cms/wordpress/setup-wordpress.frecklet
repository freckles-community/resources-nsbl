---

doc:
  short_help: installs and configures Wordpress
  help: |
    Installs and configures Wordpress.

    This frecklet creates a fully-functioning Wordpress instance,
    including MariaDB database, Apache webserver, Let's encrypt cert (optional).

args:
  domain:
    doc:
      short_help: the domain/ip address that site listens to
    required: false
    default: localhost
    type: string
    cli:
      metavar: HOSTNAME
  wp_path:
    doc:
      short_help: the path to the wordpress project folder, needs to contain the folder 'wp-content'
      help: |
        The path to the wordpress project folder.

        Contain the config file ('wp-config.php'), and the content
        folder ('wp-content').
    type: string
    required: false
    cli:
      metavar: PATH
  letsencrypt_email:
    doc:
      short_help: the email to be used with letsencrypt
    required: false
    type: string
    cli:
      metavar: EMAIL
  wp_user:
    doc:
      short_help: the user who owns the project files
    type: string
    required: false
    default: www-data
    cli:
      metavar: USER
  wp_title:
    doc:
      short_help: the name of the wordpress instance
    type: string
    required: true
    cli:
      metavar: TITLE
  wp_admin_user:
    doc:
      short_help: the name of the wordpress admin user
    type: string
    required: false
    default: admin
    cli:
      metavar: USERNAME
  wp_admin_email:
    doc:
      short_help: the email of the wordpress admin user
    required: true
    cli:
      metavar: EMAIL
  wp_admin_password:
    doc:
      short_help: the password of the wordpress admin user
    required: false
    default: "change_me"
    cli:
      metavar: PASSWORD
  wp_group:
    doc:
      short_help: the group who owns the project files
    type: string
    required: false
    default: www-data
    cli:
      metavar: GROUP
  wp_webserver_user:
    doc:
      short_help: the user who runs the Apache webserver (optional)
    type: string
    required: false
    cli:
      metavar: USER
  wp_webserver_group:
    doc:
      short_help: the group who runs the Apache webserver (optional)
    type: string
    required: false
    cli:
      metavar: GROUP
  wp_db_user:
    doc:
      short_help: the user who owns the project files
    type: string
    required: false
    default: wordpress
    cli:
      show_default: true
      metavar: DB_USER
  wp_db_password:
    doc:
      short_help: the password for the database
    type: string
    required: true
    default: wordpress_password
    cli:
      show_default: true
  wp_db_import:
    doc:
      short_help: whether to import a sql dump
    type: boolean
    default: false
    required: false
    cli:
      is_flag: true
  wp_db_dump_file:
    doc:
      short_help: an (optional) database dump file
    type: string
    required: false
    cli:
      metavar: PATH
  wp_db_name:
    doc:
      short_help: the name of the database to use
    type: string
    required: false
    default: wp_database
    cli:
      show_default: true
      metavar: DB_NAME
  wp_db_table_prefix:
    doc:
      short_help: the wordpress db table prefix
    type: string
    default: wp_
    required: false
    cli:
      show_default: true
      metavar: DB_PREFIX
  wp_plugins:
    doc:
      short_help: a list of plugins to install
    type: list
    schema:
      type: dict
      schema:
        name:
          type: string
        zip:
          type: string
        url:
          type: string
        activate:
          type: boolean
        force:
          type: boolean
    required: false
    cli:
      metavar: PLUGIN
      enabled: false
  wp_themes:
    doc:
      short_help: a list of themes to install
    type: list
    schema:
      type: dict
      schema:
        name:
          type: string
          required: true
        activate:
          type: boolean
    required: false
    cli:
      metavar: THEME
      enabled: false
meta:
  tags:
    - featured-frecklecutable
    - wordpress
  freckelize:
    var_map:
      wp_path: "{{:: path ::}}"
      wp_db_dump_file: "{{:: path ::}}/{{:: vars['dump_file'] ::}}"
      # wp_db_table_prefix: "{{:: vars['wp_dbtable_prefix'] ::}}"
      # domain: "{{:: vars['domain'] ::}}"
      wp_title: "{{:: vars['title'] ::}}"

tasks:
  - install-php:
      extra_packages:
        - php-zip
        - php-curl
        - php-gd
        - php-mbstring
        - php-mcrypt
        - php-xml
        - php-xmlrpc
        - php-mysql
        - libapache2-mod-php
  - setup-webserver:
      webserver_user: "{{:: wp_webserver_user | default(omit) ::}}"
      webserver_group: "{{:: wp_webserver_group | default(omit) ::}}"
      webserver: apache
      domain: "{{:: domain ::}}"
      use_https: "{{:: letsencrypt_email | false_if_empty ::}}"
      email: "{{:: letsencrypt_email | default(omit) ::}}"
      document_root: "/var/www/wordpress"
      webserver_extra_vars:
        apache:
          mods_enabled:
            - php7.0.load
            - mpm-prefork
          mods_disabled:
            - mpm-event
  - prepare-mariadb-database:
      db_import: "{{:: wp_db_import ::}}"
      db_dump_file: "{{:: wp_db_dump_file | default(omit) ::}}"
      db_name: "{{:: wp_db_name ::}}"
      db_user: "{{:: wp_db_user ::}}"
      db_user_password: "{{:: wp_db_password ::}}"
  - create-folder:
      path: "{{:: wp_path | default(omit) ::}}"
      owner: "{{:: wp_user | default(omit) ::}}"
      group: "{{:: wp_group | default(omit) ::}}"
      mode: "0775"
      become_user: root
#      task.__skip__: "{{:: wp_path | true_if_empty ::}}"
  - create-folder:
      path: "/var/www/wordpress"
      owner: "{{:: wp_webserver_user | default(omit) ::}}"
      group: "{{:: wp_webserver_group | default(omit) ::}}"
      mode: "0775"
      become_user: root
  - task:
      command: file
      type: ansible-module
      __msg__: "linking '{{:: wp_path ::}}' -> '/var/www/wordpress'"
      __skip__: "{{:: wp_path | true_if_empty ::}}"
      become: true
    vars:
      src: "{{:: wp_path ::}}"
      dest: "/var/www/wordpress/wp-content"
      state: link
      owner: "{{:: wp_webserver_user | default(omit) ::}}"
      group: "{{:: wp_webserver_group | default(omit) ::}}"
  - task:
      command: oefenweb.wordpress
      type: ansible-role
      become: true
      include-type: import
    vars:
      wordpress_installs:
        - name: wordpress
          dbname: "{{:: wp_db_name ::}}"
          dbuser: "{{:: wp_db_user ::}}"
          dbpass: "{{:: wp_db_password ::}}"
          dbhost: "localhost"
          dbprefix: "{{:: wp_db_table_prefix ::}}"
          path: "/var/www/wordpress"
          owner: "{{:: wp_user | default(omit) ::}}"
          group: "{{:: wp_group | default(omit) ::}}"
          url: "{{:: letsencrypt_email | false_if_empty | string_for_boolean('https', 'http') ::}}://{{:: domain ::}}"
          title: "{{:: wp_title ::}}"
          admin_name: "{{:: wp_admin_user ::}}"
          admin_email: "{{:: wp_admin_email ::}}"
          admin_password: "{{:: wp_admin_password ::}}"
          plugins: "{{:: wp_plugins | default([]) ::}}"
          themes: "{{:: wp_themes | default([]) ::}}"
          users: {}
          options: []

